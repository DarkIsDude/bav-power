import decimal
from django.db.models.aggregates import Count, Sum
from django.shortcuts import render
from django.contrib.auth.decorators import permission_required

from bav.models import BAV, Sale, SaleItem, Seller

@permission_required('bav.view_bav')
def index(request, bav_id):
    bav = BAV.objects.get(pk=bav_id)

    payment_methods = []
    for payment_method in Sale.objects.values('payment_method').distinct():
        payment_method['count'] = Sale.objects.filter(payment_method=payment_method['payment_method']).count()
        payment_method['sum'] = SaleItem.objects.filter(sale__payment_method=payment_method['payment_method']).aggregate(Sum('price'))['price__sum']
        payment_methods.append(payment_method)

    sellers = []
    for seller in Seller.objects.all().order_by('slug'):
        s = {}
        s['firstname'] = seller.firstname
        s['lastname'] = seller.lastname
        s['slug'] = seller.slug
        s['is_adherent'] = seller.is_adherent
        s['is_board_member'] = seller.is_board_member
        s['amount'] = SaleItem.objects.filter(seller=seller).aggregate(Sum('price'))['price__sum']
        s['items'] = SaleItem.objects.filter(seller=seller).values('article_id').order_by('article_id')

        if seller.is_adherent:
            s['profit'] = s['amount'] * decimal.Decimal(0.85)
        elif seller.is_board_member:
            s['profit'] = s['amount'] * decimal.Decimal(0.90)
        else:
            s['profit'] = s['amount'] * decimal.Decimal(0.80)

        sellers.append(s)

    return render(request, 'stats/index.html', {
        'bav': bav,
        'payment_methods': payment_methods,
        'sellers': sellers,
    })
