from django.urls import path

from . import views

app_name = 'stats'
urlpatterns = [
    path('bav/<int:bav_id>', views.index, name='index'),
]
