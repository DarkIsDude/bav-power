dj-database-url==2.1.0
Django==5.0.1
django-debug-toolbar==4.2.0
psycopg2-binary==2.9.9
python-dotenv==1.0.1
sqlparse==0.4.4
tzdata==2024.1
whitenoise==6.6.0
