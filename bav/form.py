from django import forms
from django.forms import inlineformset_factory

from .models import BAV, Sale, SaleItem

class SaleForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.fields['client'].widget.attrs['class'] = 'form-control'
        self.fields['is_adherent'].widget.attrs['class'] = 'form-check-input'
        self.fields['is_board_member'].widget.attrs['class'] = 'form-check-input'

    class Meta:
        model = Sale
        fields = ['client', 'is_adherent', 'is_board_member']

class PaymentForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.fields['payment_method'].widget.attrs['class'] = 'form-control'

    class Meta:
        model = Sale
        fields = ['payment_method']

class SaleItemForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.fields['seller'].widget.attrs['class'] = 'form-control'
        self.fields['price'].widget.attrs['class'] = 'form-control'
        self.fields['article_id'].widget.attrs['class'] = 'form-control'

    class Meta:
        model = SaleItem
        fields = ['seller', 'price', 'article_id']
