from django.contrib import admin
from .models import BAV, Seller, Sale, SaleItem

class SaleItemInline(admin.TabularInline):
    model = SaleItem
    extra = 3

class SaleAdmin(admin.ModelAdmin):
    fieldsets = [
        (None, {'fields': ['bav', 'date', 'client', 'is_adherent', 'is_board_member', 'payment_method']}),
    ]
    inlines = [SaleItemInline]
    list_display = ('id', 'bav', 'date', 'client', 'is_adherent', 'is_board_member')
    list_filter = ['bav', 'date', 'client', 'is_adherent', 'is_board_member']
    search_fields = ['client']

admin.site.register(BAV)
admin.site.register(Seller)
admin.site.register(Sale, SaleAdmin)
admin.site.register(SaleItem)
