import datetime
from django.forms import inlineformset_factory
from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.contrib.auth.decorators import permission_required
from django.urls import reverse
from django.views.generic.detail import DetailView
from django.views.generic.list import ListView
from django.contrib.auth.mixins import PermissionRequiredMixin

from .form import PaymentForm, SaleForm, SaleItemForm

from .models import BAV, Sale, SaleItem

def index(request):
    return HttpResponseRedirect(reverse('bav:bav_list'))

class BAVListView(PermissionRequiredMixin, ListView):
    model = BAV
    permission_required = 'bav.view_bav'

@permission_required('bav.add_sale')
def sell(request, bav_id):
    bav = BAV.objects.get(pk=bav_id)
    SaleItemFormSet = inlineformset_factory(Sale, SaleItem, form=SaleItemForm, extra=50, can_delete=False)

    form = SaleForm()
    sale = Sale()
    formset = SaleItemFormSet(instance=sale)

    if request.method == 'POST':
        form = SaleForm(request.POST)

        if form.is_valid():
            sale = form.save(commit=False)
            sale.bav = bav
            sale.date = datetime.datetime.now()
            formset = SaleItemFormSet(request.POST, instance=sale)

            if formset.is_valid():
                sale.save()
                formset.save()

                return HttpResponseRedirect(reverse('bav:sale_detail', args=(sale.id,)))

    return render(request, 'bav/sell.html', {'bav': bav, 'form': form, 'formset': formset})

@permission_required('bav.change_sale')
def pay(request, sale_id):
    sale = Sale.objects.get(pk=sale_id)
    form = PaymentForm(instance=sale)

    if request.method == 'POST':
        form = PaymentForm(request.POST)

        if form.is_valid():
            sale.payment_method = form.cleaned_data['payment_method']
            sale.save()

            return HttpResponseRedirect(reverse('bav:sale_detail', args=(sale.id,)))

    return render(request, 'bav/pay.html', {'sale': sale, 'form': form })

@permission_required('bav.change_sale')
def bav_pay(request, bav_id):
    bav = BAV.objects.get(pk=bav_id)
    sales = Sale.objects.filter(bav=bav, payment_method__isnull=True)

    return render(request, 'bav/bav_pay.html', {'bav': bav, 'sales': sales})

class SaleDetailView(PermissionRequiredMixin, DetailView):
    model = Sale
    permission_required = 'bav.view_sale'
