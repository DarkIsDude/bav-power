from django.db import models

class BAV(models.Model):
    name = models.CharField(max_length=30, unique=True)

    def __str__(self):
        return '{}'.format(self.name)

class Seller(models.Model):
    firstname = models.CharField(max_length=30)
    lastname = models.CharField(max_length=30)
    email = models.EmailField()
    slug = models.SlugField(max_length=5, unique=True)
    is_adherent = models.BooleanField()
    is_board_member = models.BooleanField()

    def __str__(self):
        return '{} {}'.format(self.firstname, self.lastname)

class Sale(models.Model):
    bav = models.ForeignKey(BAV, on_delete=models.CASCADE)
    date = models.DateTimeField()
    client = models.CharField(max_length=30)
    is_adherent = models.BooleanField()
    is_board_member = models.BooleanField()
    payment_method = models.CharField(max_length=30, null=True, blank=True)

    def __str__(self):
        return '{} - {} - {}'.format(self.id, self.bav.name, self.client, )

class SaleItem(models.Model):
    sale = models.ForeignKey(Sale, on_delete=models.CASCADE)
    seller = models.ForeignKey(Seller, on_delete=models.CASCADE)
    price = models.DecimalField(max_digits=10, decimal_places=2)
    article_id = models.PositiveSmallIntegerField()

    def __str__(self):
        return '{} - {} - {}'.format(self.id, self.seller, self.price)
