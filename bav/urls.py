from django.urls import path

from . import views

app_name = 'bav'
urlpatterns = [
    path('', views.index, name='index'),
    path('bav', views.BAVListView.as_view(), name='bav_list'),
    path('bav/<int:bav_id>/sell', views.sell, name='sell'),
    path('bav/<int:bav_id>/pay', views.bav_pay, name='bav_pay'),
    path('sale/<int:pk>', views.SaleDetailView.as_view(), name='sale_detail'),
    path('sale/<int:sale_id>/pay', views.pay, name='pay'),
]
