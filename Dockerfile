FROM alpine

WORKDIR /app

RUN set -xe;

COPY . .

RUN apk add --no-cache python3 py3-pip libpq-dev python3-dev; \
  pip3 install --no-cache-dir -r /app/requirements.txt --break-system-packages; \
  addgroup -g 1000 appuser; \
  adduser -u 1000 -G appuser -D -h /app appuser; \
  chown -R appuser:appuser /app;

USER appuser
EXPOSE 8000/tcp

CMD [ "python3", "/app/manage.py", "runserver", "0.0.0.0:8000" ]
