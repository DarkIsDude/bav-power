# Bav Power

## Prerequist

- Python > 3.10
- VirtualEnv
- Django

## Setup

```bash
virtualenv env
source ./env/bin/activate

pip install -r requirements.txt
python manage.py runserver
```

Copy `.env.dist` to `.env` and custom it.
